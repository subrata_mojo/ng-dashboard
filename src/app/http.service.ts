import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IWidget } from './interfaces';
import { URL } from './urls';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient
  ) { }

  public async get(url: string): Promise<IWidget[]> {
    try {
      const response = await this.http
      .get(url)
      .toPromise();
      return response as IWidget[];
    } catch {

    }
  }
}
