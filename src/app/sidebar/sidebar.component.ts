import { Component, OnInit, Input } from '@angular/core';
import { ISidebar } from '../interfaces';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.styl']
})
export class SidebarComponent implements OnInit {

  @Input() sidebarConfig: Array<ISidebar> = [];

  constructor() { }

  ngOnInit() {
  }

}
