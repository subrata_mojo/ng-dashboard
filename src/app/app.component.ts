import { Component, OnInit } from '@angular/core';
import { HttpService } from './http.service';
import { URL } from './urls';
import { ISidebar } from './interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent implements OnInit {

  public sidebarData: Array<ISidebar> = [];

  constructor(
    private Http: HttpService
  ) {}

  ngOnInit() {
    this.getSidebarData();
  }

  protected getSidebarData = (): void => {
    this.Http.get(URL.SIDEBAR).then((response: any) => {
      this.sidebarData = response.data;
    });
  }
}
