import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { IWidget } from '../interfaces';
import { URL } from '../urls';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.styl']
})
export class DashboardComponent implements OnInit {

  public widgetProp: Array<IWidget> = [];

  public statisticsChartProp: any = {
    data: [],
    labels: [],
    options: {
      responsive: true
    },
    colors: [],
    legend: true,
    type: 'line'
  };

  constructor(
    private Http: HttpService
  ) { }

  ngOnInit() {
    this.Http.get(URL.WIDGET).then((response: any) => {
      console.log(response);
      this.widgetProp = response.data;
    });
    this.getStatisticsData();
  }

  protected getStatisticsData = () => {
    this.Http.get(URL.STATISTICS).then((response: any) => {
      console.log(response);
      this.setStatisticsChartData(response.data);
    });
  }

  protected setStatisticsChartData = (params: any) => {
    const chartDataProductSold: Array<number>= [];
    const chartDataTotalViews: Array<number> = [];
    params.forEach((elem: any) => {
      this.statisticsChartProp.labels.push(elem.month);
      chartDataProductSold.push(elem.product_sold);
      chartDataTotalViews.push(elem.total_views);
    });
    this.statisticsChartProp.data.push({
      data: chartDataProductSold,
      label: 'Product Sold'
    });
    this.statisticsChartProp.data.push({
      data: chartDataTotalViews,
      label: 'Total Views'
    });
    this.statisticsChartProp.colors.push(
      {
        backgroundColor: 'rgba(85, 216, 254, 0.2)',
        borderColor: 'rgba(85, 216, 254,1)',
        pointBackgroundColor: 'rgba(85, 216, 254,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(85, 216, 254,0.8)'
      },
      {
        backgroundColor: 'rgba(164, 161, 251,0.2)',
        borderColor: 'rgba(164, 161, 251,1)',
        pointBackgroundColor: 'rgba(164, 161, 251,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(164, 161, 251,1)'
      }
    );
    console.log(this.statisticsChartProp);
  }

}
