export interface IWidget {
    name: string;
    value: string;
    change: string;
    change_code: string;
}

export interface IStatistics {
    month: string;
    stats: number;
}

export interface ISidebar {
    title: string;
    icon: string;
}
