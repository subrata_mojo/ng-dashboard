export const URL = {
    WIDGET: 'http://localhost:4200/assets/json/widget.json',
    STATISTICS: 'http://localhost:4200/assets/json/statistics.json',
    SIDEBAR: 'http://localhost:4200/assets/json/navigation.json'
};
